package com.myApp.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.myApp.client.BookEntity;
import com.myApp.client.service.MyAppService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.query.Query;

import java.util.List;

public class MyAppServiceImpl extends RemoteServiceServlet implements MyAppService {
    private SessionFactory sessionFactory;
    private Session session;
    private boolean filterAct = false;
    private  String filterField = "";
    private  String filterText = "";

    public MyAppServiceImpl(){
        sessionFactory = HibernateUtil.getSessionFactory();
        session = sessionFactory.openSession();
    }
    public List<BookEntity> getBooks(boolean filterAct, String filterField, String filterText){
        this.filterAct = filterAct;
        this.filterField = filterField;
        this.filterText = filterText;

        session.beginTransaction();
        Query query;
        if (filterAct) {
            if (!filterField.equals("year")){
                query =  session.createQuery("FROM BookEntity WHERE " + filterField + " LIKE :filtr");
                query.setParameter("filtr", "%" + filterText + "%");
            } else {
                query =  session.createQuery("FROM BookEntity WHERE year = " + filterText);
            }
        } else {
            query =  session.createQuery("from BookEntity");
        }
        session.getTransaction().commit();
        return query.list();
    }

    public List<BookEntity> addBook(BookEntity book){
        session.beginTransaction();
        session.save(book);
        session.getTransaction().commit();
        return getBooks(filterAct, filterField, filterText);
    }

    public List<BookEntity> removeBooks(List<Integer> nums) {
        session.beginTransaction();

        String hql = "delete from BookEntity where id in (:ids)";
        session.createQuery(hql).setParameterList("ids", nums).executeUpdate();

        session.getTransaction().commit();
        return getBooks(filterAct, filterField, filterText);
    }

    public List<BookEntity> replaceBooks(int num, BookEntity book) {
        session.beginTransaction();
        session.merge(book);
        session.getTransaction().commit();
        return getBooks(filterAct, filterField, filterText);
    }
}