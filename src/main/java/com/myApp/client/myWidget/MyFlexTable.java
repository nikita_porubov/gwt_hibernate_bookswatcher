package com.myApp.client.myWidget;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.myApp.client.BookEntity;
import com.myApp.client.style.Resources;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
/*
 *
 * Виджет FlexTable для отображения книг
 *
 * */
public class MyFlexTable extends FlexTable {
    // Listener для события "Нажата кнопка редактировать книгу"
    private editBook editBookListener;
    public interface editBook{
        void change(int num);
    }
    public void setEditBookHandler(editBook listener){
        editBookListener = listener;
    }

    // Создаем таблицу
    public MyFlexTable(){
        removeAllRowsAndSetHead();
    }

    // Удаем строки и втавляем шапку
    public void removeAllRowsAndSetHead(){
        removeAllRows();

        getRowFormatter().addStyleName(0, "watchListHeader");
        getRowFormatter().addStyleName(0, "row");

        getCellFormatter().addStyleName(0, 0, "col-sm-1");
        getCellFormatter().addStyleName(0, 1, "col-sm-1");
        getCellFormatter().addStyleName(0, 2, "col-sm-3");
        getCellFormatter().addStyleName(0, 3, "col-sm-3");
        getCellFormatter().addStyleName(0, 4, "col-sm-1");
        getCellFormatter().addStyleName(0, 5, "col-sm-2");
        getCellFormatter().addStyleName(0, 6, "col-sm-1");

        setText(0, 0, "Remove");
        setText(0, 1, "#");
        setText(0, 2, "Author");
        setText(0, 3, "Title");
        setText(0, 4, "Year");
        setText(0, 5, "Timestamp");
        setText(0, 6, "Edit");
    }

    // Формат для отображения даты
    DateTimeFormat dateFormat = DateTimeFormat.getFormat(
            DateTimeFormat.PredefinedFormat.DATE_TIME_MEDIUM);
    // Вставляем строки
    public void setRow(int row, BookEntity book){
        Button button = new Button("Edit");
        button.addClickHandler(event -> editBookListener.change(row-1));
        setWidget(row, 0, new CheckBox());
        setText(row, 1, "" + row);
        setText(row, 2, book.getAuthor());
        setText(row, 3, book.getTitle());
        setText(row, 4, "" + book.getYear());
        setText(row, 5, dateFormat.format(book.getTimestamp()));
        setWidget(row, 6, button);

       // getCellFormatter().addStyleName(row, 0, "watchListRemovecol-smumn");
        getRowFormatter().addStyleName(row, "row");
        getCellFormatter().addStyleName(row, 0, "col-sm-1");
        getCellFormatter().addStyleName(row, 1, "col-sm-1");
        getCellFormatter().addStyleName(row, 2, "col-sm-3");
        getCellFormatter().addStyleName(row, 3, "col-sm-3");
        getCellFormatter().addStyleName(row, 4, "col-sm-1");
        getCellFormatter().addStyleName(row, 5, "col-sm-2");
        getCellFormatter().addStyleName(row, 6, "col-sm-1");
    }
    // Определям номера книг для удаления
    public List<Integer> removeBooks(){
        return IntStream.range(1, getRowCount())
                .filter(i -> ((CheckBox)this.getWidget(i, 0)).getValue())
                .mapToObj(i -> new Integer(i-1))
                .collect(Collectors.toList());
    }
}