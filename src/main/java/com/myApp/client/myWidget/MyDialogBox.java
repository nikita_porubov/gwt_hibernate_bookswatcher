package com.myApp.client.myWidget;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.myApp.client.BookEntity;

/*
*
* Виджет DialogBox для добавления/редактирования книги
*
* */

public class MyDialogBox extends DialogBox {
    // Виджеты
    private Label authorDialogBoxLabel = new Label("Author: ");
    private TextBox authorDialogBoxTextBox = new TextBox();
    private Label titleDialogBoxLabel = new Label("Title: ");
    private TextBox titleDialogBoxTextBox = new TextBox();
    private Label yearDialogBoxLabel = new Label("Year: ");
    private TextBox yearDialogBoxTextBox = new TextBox();
    private Button cancelButton = new Button("Close");
    private Button addButton = new Button("Add");
    private Button editButton = new Button("Edit");

    // Создаем окно
    public MyDialogBox(){
        setAutoHideEnabled(true);
        setModal(true);
        setGlassEnabled(true);
        setAnimationEnabled(true);
        setPopupPosition(300, 300);

        VerticalPanel dock = new VerticalPanel();
        dock.add(authorDialogBoxLabel);
        dock.add(authorDialogBoxTextBox);
        dock.add(titleDialogBoxLabel);
        dock.add(titleDialogBoxTextBox);
        dock.add(yearDialogBoxLabel);
        dock.add(yearDialogBoxTextBox);
        dock.add(addButton);

        HorizontalPanel buttonsDock = new HorizontalPanel();
        buttonsDock.add(editButton);
        buttonsDock.add(cancelButton);

        dock.add(buttonsDock);

        addButton.addClickHandler(event ->{
            final String Author = authorDialogBoxTextBox.getText().trim();
            final String Title = titleDialogBoxTextBox.getText().trim();
            final String Year = yearDialogBoxTextBox.getText().trim();

            if (!Author.matches("^[0-9A-Za-z\\.\\s]{1,20}$")) {
                Window.alert("'" + Author + "' is not a valid Author.");
                authorDialogBoxTextBox.selectAll();
                return;
            }
            if (!Title.matches("^[0-9A-Za-z\\.\\s]{1,20}$")) {
                Window.alert("'" + Title + "' is not a valid Title.");
                titleDialogBoxTextBox.selectAll();
                return;
            }
            if (!Year.matches("^[0-9]{1,4}$")) {
                Window.alert("'" + Year + "' is not a valid Year.");
                yearDialogBoxTextBox.selectAll();
                return;
            }
            addBookListener.pass(new BookEntity(Author, Title, Integer.valueOf(Year)));
        });
        cancelButton.addClickHandler(event -> hide());

        setWidget(dock);
    }
    // Listener для события "Добавить книгу"
    private AddBookListener addBookListener;
    public interface AddBookListener{
        void pass(BookEntity book);
    }
    public void setAddBookListener(AddBookListener listener){
        addBookListener = listener;
    }
    // Открытие диалога "Добавить книгу"
    public void openAddDialog(){
        this.setText("Add book");

        authorDialogBoxTextBox.setText("");
        titleDialogBoxTextBox.setText("");
        yearDialogBoxTextBox.setText("");

        addButton.setVisible(true);
        editButton.setVisible(false);

        show();
    }
    // Listener для события "Редактировать книгу"
    private EditBookListener editBookListener;
    public interface EditBookListener{
        void pass(int num, BookEntity book);
    }
    public void setEditBookListener(EditBookListener listener){
        editBookListener = listener;
    }
    // Открытие диалога "Редактировать книгу"
    private HandlerRegistration lastEditHandler;
    public void openEditDialog(int num, BookEntity book){
        this.setText("Edit book");
        authorDialogBoxTextBox.setText(book.getAuthor());
        titleDialogBoxTextBox.setText(book.getTitle());
        yearDialogBoxTextBox.setText("" + book.getYear());

        addButton.setVisible(false);
        editButton.setVisible(true);

        if (lastEditHandler != null) lastEditHandler.removeHandler();
        lastEditHandler = editButton.addClickHandler(event ->{
            final String Author = authorDialogBoxTextBox.getText().trim();
            final String Title = titleDialogBoxTextBox.getText().trim();
            final String Year = yearDialogBoxTextBox.getText().trim();

            if (!Author.matches("^[0-9A-Za-z\\.\\s]{1,20}$")) {
                Window.alert("'" + Author + "' is not a valid Author.");
                authorDialogBoxTextBox.selectAll();
                return;
            }
            if (!Title.matches("^[0-9A-Za-z\\.\\s]{1,20}$")) {
                Window.alert("'" + Title + "' is not a valid Title.");
                titleDialogBoxTextBox.selectAll();
                return;
            }
            if (!Year.matches("^[0-9]{1,4}$")) {
                Window.alert("'" + Year + "' is not a valid Year.");
                yearDialogBoxTextBox.selectAll();
                return;
            }
            book.setAuthor(Author);
            book.setTitle(Title);
            book.setYear(Integer.parseInt(Year));

            editBookListener.pass(num, book);
        });
        show();
    }
}
