package com.myApp.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;


public class MyApp implements EntryPoint {

    MyAppUiBinder ui = new MyAppUiBinder();
    /**
     * Entry point method.
     */
    public void onModuleLoad() {
        // Associate the Main panel with the HTML host page.
        RootPanel.get("container").add(ui);
    }

}
