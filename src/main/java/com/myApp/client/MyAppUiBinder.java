package com.myApp.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.myApp.client.myWidget.MyDialogBox;
import com.myApp.client.myWidget.MyFlexTable;
import com.myApp.client.service.MyAppService;
import com.myApp.client.service.MyAppServiceAsync;
import com.myApp.client.style.Resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/*
 *
 * Виджет основного окна
 *
 * */
class MyAppUiBinder extends Composite {
    @UiTemplate("MyAppUiBinder.ui.xml")
    interface MySampleApplicationUiBinderUiBinder extends

    UiBinder<HTMLPanel, MyAppUiBinder> {}
    @UiField(provided = true)
    final Resources res;

    private static MySampleApplicationUiBinderUiBinder ourUiBinder
            = GWT.create(MySampleApplicationUiBinderUiBinder.class);

    private MyAppServiceAsync bookSvc = GWT.create(MyAppService.class);
    // Виджеты
    private final MyDialogBox dialog = new MyDialogBox();
    @UiField
    HTMLPanel containerHTMLPanel;
    @UiField
    HorizontalPanel filterPanel;
    @UiField
    CheckBox filterCheckBox;
    @UiField
    Label filterLabel;
    @UiField
    ListBox filterListBox;
    @UiField
    TextBox filterTextBox;
    @UiField
    Button filterApplyButton;
    @UiField
    Button filterResetButton;
    @UiField
    VerticalPanel mainPanel;
    @UiField
    MyFlexTable booksFlexTable;
    @UiField
    HorizontalPanel addPanel;
    @UiField
    Button addBooksButton;
    @UiField
    Button removeBooksButton;
    @UiField
    Label lastUpdatedLabel;
    @UiField
    Label errorMsgLabel;

    // Хранилище книг на стороне клиента
    private ArrayList<BookEntity> books;
    // Фильтрация
    private boolean filterBook = false;

    // Отображение основного окна
    public MyAppUiBinder() {
        this.res = GWT.create(Resources.class);
        res.style().ensureInjected();
        initWidget(ourUiBinder.createAndBindUi(this));

        containerHTMLPanel.addStyleName("container-fluid");
        mainPanel.addStyleName("container-fluid");
        booksFlexTable.addStyleName("container-fluid");

        addPanel.addStyleName("panel");
        filterPanel.addStyleName("panel");

        errorMsgLabel.setVisible(false);

        filterListBox.addItem("author");
        filterListBox.addItem("title");
        filterListBox.addItem("year");

        addBooksButton.addClickHandler(event -> dialog.openAddDialog());
        removeBooksButton.addClickHandler(event ->
                bookSvc.removeBooks(booksFlexTable.removeBooks()
                                .stream()
                                .map(num -> books.get(num).getId())
                                .collect(Collectors.toList())
                        , booksCallback));

        dialog.setAddBookListener(book -> bookSvc.addBook(book, booksCallback));
        dialog.setEditBookListener((n, book) -> bookSvc.replaceBooks(n, book, booksCallback));

        booksFlexTable.setEditBookHandler(num ->  dialog.openEditDialog(num, books.get(num)));

        // Фильрация (ТО DO вынести в отдельный виджет)
        filterTextBox.addKeyDownHandler(event ->{
            if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                filterBook = true;
                filterCheckBox.setValue(true);
                getBooks();
            }
        });
        filterApplyButton.addClickHandler(event -> {
            filterBook = true;
            filterCheckBox.setValue(true);
            getBooks();
        });
        filterResetButton.addClickHandler(event -> {
            filterBook = false;
            filterCheckBox.setValue(false);
            getBooks();
        });

        getBooks();
    }
    private AsyncCallback<List<BookEntity>> booksCallback = new AsyncCallback<List<BookEntity>>() {
        public void onFailure(Throwable caught) {
            errorMsgLabel.setText("Error: " + caught.toString());
            errorMsgLabel.setVisible(true);
        }

        public void onSuccess(List<BookEntity> result) {
            books = new ArrayList<>(result);

            refreshWatchList();

            errorMsgLabel.setText("");
            errorMsgLabel.setVisible(false);
        }
    };
    private void getBooks(){
        bookSvc.getBooks(filterBook, filterListBox.getSelectedItemText(), filterTextBox.getText(), booksCallback);
    }
    private void refreshWatchList() {

        booksFlexTable.removeAllRowsAndSetHead();
        IntStream.range(0, books.size())
                .forEach(i -> booksFlexTable.setRow(i + 1,  books.get(i)));

    }
}