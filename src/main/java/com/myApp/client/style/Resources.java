package com.myApp.client.style;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;

public interface Resources extends ClientBundle {
    @Source("com/myApp/client/style/MyApp.css")
    Style style();

    interface Style extends CssResource {
        String noChange();
        String positiveChange();
        String negativeChange();
        String errorMessage();
    }
}
