package com.myApp.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.myApp.client.BookEntity;

import java.util.ArrayList;
import java.util.List;

@RemoteServiceRelativePath("book")
public interface MyAppService extends RemoteService {
    List<BookEntity> getBooks(boolean filtred, String field, String filtr);
    List<BookEntity> addBook(BookEntity book);
    List<BookEntity> removeBooks(List<Integer> nums);
    List<BookEntity> replaceBooks(int num, BookEntity book);
}
