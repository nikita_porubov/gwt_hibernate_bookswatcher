package com.myApp.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.myApp.client.BookEntity;

import java.util.List;

public interface MyAppServiceAsync {
    void getBooks(boolean filtred, String field, String filtr, AsyncCallback<List<BookEntity>> callback);
    void addBook(BookEntity book, AsyncCallback<List<BookEntity>> callback);
    void removeBooks(List<Integer> nums, AsyncCallback<List<BookEntity>> callback);
    void replaceBooks(int num, BookEntity book, AsyncCallback<List<BookEntity>> async);
}

