CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(45) NOT NULL,
  `title` varchar(45) NOT NULL,
  `year` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

DELETE FROM Book;

INSERT INTO book (author, title, year) VALUES ("Stephen King", "Carrie", 1974);
INSERT INTO book (author, title, year) VALUES ("Stephen King", "The Green Mile", 1996);
INSERT INTO book (author, title, year) VALUES ("Stephen King", "Misery", 1987);
INSERT INTO book (author, title, year) VALUES ("John Ronald Reuel Tolkien", "The Hobbit", 1937);
INSERT INTO book (author, title, year) VALUES ("John Ronald Reuel Tolkien", "The Fellowship of the Ring", 1954);

